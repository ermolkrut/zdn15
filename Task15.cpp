#include <iostream>

using namespace std;

void PrintNumbers(int N, bool isOdd)

{
	for (int i = isOdd; i <= N; i++)
	{
		if (isOdd)
		{
			if (i % 2 > 0)
			{
				cout << i << "\n";
			}
		}
		else
		{
			if (i % 2 == 0)
			{
				cout << i << "\n";
			}
		}
	}
}

int main()
{

	PrintNumbers(10, false);
	PrintNumbers(10, true);
	cout << endl;

	return 0;
}